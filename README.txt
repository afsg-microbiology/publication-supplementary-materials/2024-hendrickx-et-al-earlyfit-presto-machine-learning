***R code for Earlyfit PRESTO Machine learning***

***Title: ‘A multi-omics machine learning classifier for outgrowth of cow’s milk allergy in children’***

***Creators***
Diana M. Hendrickx 1, Mariyana V. Savova 2, Pingping Zhu 2, Ran An 1,+, Sjef Boeren 3, Kelly Klomp 1, Sumanth K. Mutte 3, 
PRESTO study team*, Harm Wopereis 4, Renate G. van der Molen 5, Amy C. Harms 2 and Clara Belzer
1 Laboratory of Microbiology, Wageningen University, Wageningen, The Netherlands
2 Metabolomics and Analytics Centre, Leiden Academic Centre for Drug Research, Leiden University, Leiden, The Netherlands
3 Laboratory of Biochemistry, Wageningen University, Wageningen, The Netherlands
4 Danone Nutricia Research, Utrecht, The Netherlands
5 Department of Laboratory Medicine, Laboratory of Medical Immunology, Radboudumc, Nijmegen, The Neth-erlands

+ Current address: Department of Food Science and Technology, School of Agriculture and Biology, Shanghai Jiao Tong University, Shanghai, China

* Correspondence: clara.belzer@wur.nl

* PRESTO study team: A list of authors and their affiliations will be published together with the manuscript.

***Related publication***
Publication pending

***General Introduction***
This folder contains the R scripts for the multi-omics machine learning classifier for outgrowth of cow’s milk allergy developed within the Earlyfit project.
This study was part of the EARLYFIT project (Partnership programme NWO Domain AES-Danone Nutricia Research), funded by the Dutch Research Council (NWO) and Danone Nutricia Research (project number: 16490).

***Data availability***
Raw sequencing data are available in the European Nucleotide Archive (ENA) (http://www.ebi.ac.uk/ena) under accession number PRJEB56782.
Raw proteomics data are available from ProteomeXchange via the PRIDE partner repository under accession number PXD037190. 
Metabolomics data are available from MetaboLights (https://www.ebi.ac.uk/metabolights/ ) under accession number MTBLS8954 . 
Olink immune data are available as supplementary material (Gitlab folder) from another manuscript (biorxiv XXXXXX). 
Clinical data are available from Danone Nutricia Research upon reasonable request (contact: Harm Wopereis, Harm.Wopereis@danone.com).

