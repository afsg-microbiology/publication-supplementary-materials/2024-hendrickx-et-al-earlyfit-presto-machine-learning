***General***
In all models, the positive class is "no outgrowth of cow's milk allergy at 12 months" (persistent cow's milk allergy).
R version 4.2.1

***Folder pre-processing for ML***

preprocess_for_ML_clinical.R: Extra pre-processing steps to put clinical data in the right format for ML.

preprocess_for_ML_16SrRNAseq.R: Extra pre-processing steps to put 16S rRNA gene sequencing data in the right format for ML (at the genus level).

preprocess_for_ML_metaproteomics.R: Extra pre-processing steps to put metaproteomics data in the right format for ML (at the level of protein groups).

preprocess_for_ML_immune.R: Extra pre-processing steps to put immune data in the right format for ML.

preprocess_for_ML_metabolomics.R: Extra pre-processing steps to put metabolomics data in the right format for ML.


***Folder train-test splitting***

split_subjects.R: Stratified splitting of subjects based on tolerance to cow's milk at 12m. 
We use 2/3 of the subjects for training, and 1/3 for testing. 
Data from the same subject are kept together, so that they belong or to the training set, or to the test set.
We repeat this 5 times. We use the same train-test splits for each type of data.

train_test_clinical.R: Generate training and test sets for the clinical data.

train_test_16SrRNAseq.R: Generate training and test sets for the 16S rRNA gene sequencing data.

train_test_metaproteomics.R: Generate training and test sets for the metaproteomics data.

train_test_immune.R: Generate training and test sets for the immune data.

train_test_metabolomics.R: Generate training and test sets for the metabolomics data.


***Folder RF_with_default_settings***

RF_default_clinical_all.R: Run random forests on clinical data (all visits) applying the default settings.

RF_default_clinical_0M.R: Run random forests on clinical data from the baseline visit applying the default settings. 

RF_default_clinical_6M.R: Run random forests on clinical data from visit 6 months applying the default settings.

RF_default_clinical_12M.R: Run random forests on clinical data from visit 12 months applying the default settings.

RF_default_16SrRNAseq_all.R: Run random forests on 16S rRNA gene sequencing data (all visits) applying the default settings.

RF_default_16SrRNAseq_0M.R: Run random forests on 16S rRNA gene sequencing data from the baseline visit applying the default settings.

RF_default_16SrRNAseq_6M.R: Run random forests on 16S rRNA gene sequencing data from the visit 6 months applying the default settings. 

RF_default_16SrRNAseq_12M.R: Run random forests on 16S rRNA gene sequencing data from the visit 12 months applying the default settings. 

RF_default_microb_metaproteomics_all.R: Run random forests on microbial metaproteomics data (all visits) applying the default settings. 

RF_default_microb_metaproteomics_0M.R: Run random forests on microbial metaproteomics data of the baseline visit applying the default settings. 

RF_default_microb_metaproteomics_6M.R: Run random forests on microbial metaproteomics data of visit 6 months applying the default settings.

RF_default_microb_metaproteomics_12M.R: Run random forests on microbial metaproteomics data of visit 12 months applying the default settings.

RF_default_human_proteomics_all.R: Run random forests on human proteomics data (all visits) applying the default settings. 

RF_default_human_proteomics_0M.R: Run random forests on human proteomics data of the baseline visit applying the default settings.

RF_default_human_proteomics_6M.R: Run random forests on human proteomics data of visit 6 months applying the default settings.

RF_default_human_proteomics_12M.R: Run random forests on human proteomics data of visit 12 months applying the default settings.

RF_default_immune_all.R: Run random forests on immune data (all visits) applying the default settings.

RF_default_immune_0M.R: Run random forests on immune data of the baseline visit applying the default settings.

RF_default_immune_6M.R: Run random forests on immune data of visit 6 months applying the default settings.

RF_default_immune_12M.R: Run random forests on immune data of visit 6 months applying the default settings.

RF_default_metabolomics_polarN_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits) applying the default settings.

RF_default_metabolomics_polarN_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for the baseline visit applying the default settings.

RF_default_metabolomics_polarN_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 6 months applying the default settings.

RF_default_metabolomics_polarN_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 12 months applying the default settings.

RF_default_metabolomics_polarP_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits) applying the default settings.

RF_default_metabolomics_polarP_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from the baseline visit applying the default settings. 

RF_default_metabolomics_polarP_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 6 months applying the default settings.  

RF_default_metabolomics_polarP_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 12 months applying the default settings.  

RF_default_metabolomics_lipid_all.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids (all visits) applying the default settings. 

RF_default_metabolomics_lipid_0M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for the baseline visit applying the default settings. 

RF_default_metabolomics_lipid_6M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 6 months applying the default settings. 

RF_default_metabolomics_lipid_12M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 12 months applying the default settings. 

RF_default_combine_models_all_visits.R: Combine the predictions of the models using all visits and default settings.

RF_default_combine_models_per_visit.R: Combine the predictions of the models per visit and default settings.


***folder influence_oversampling***

RF_oversampling_clinical_all.R: Run random forests on clinical data (all visits) applying the default settings and using oversampling.

RF_oversampling_clinical_0M.R: Run random forests on clinical data from the baseline visit applying the default settings and using oversampling.

RF_oversampling_clinical_6M.R: Run random forests on clinical data from visit 6 months applying the default settings and using oversampling.

RF_oversampling_clinical_12M.R: Run random forests on clinical data from visit 12 months applying the default settings and using oversampling.

RF_oversampling_16SrRNAseq_all.R: Run random forests on 16S rRNA gene sequencing data (all visits) applying the default settings and using oversampling.

RF_oversampling_16SrRNAseq_0M.R: Run random forests on 16S rRNA gene sequencing data from the baseline visit applying the default settings and using oversampling.

RF_oversampling_16SrRNAseq_6M.R: Run random forests on 16S rRNA gene sequencing data from the visit 6 months applying the default settings and using oversampling.

RF_oversampling_16SrRNAseq_12M.R: Run random forests on 16S rRNA gene sequencing data from the visit 12 months applying the default settings and using oversampling.

RF_oversampling_microb_metaproteomics_all.R: Run random forests on microbial metaproteomics data (all visits) applying the default settings and using oversampling. 

RF_oversampling_microb_metaproteomics_0M.R: Run random forests on microbial metaproteomics data of the baseline visit applying the default settings and using oversampling.

RF_oversampling_microb_metaproteomics_6M.R: Run random forests on microbial metaproteomics data of visit 6 months applying the default settings and using oversampling.

RF_oversampling_microb_metaproteomics_12M.R: Run random forests on microbial metaproteomics data of visit 12 months applying the default settings and using oversampling.

RF_oversampling_human_proteomics_all.R: Run random forests on human proteomics data (all visits) applying the default settings and using oversampling.

RF_oversampling_human_proteomics_0M.R: Run random forests on human proteomics data of the baseline visit applying the default settings and using oversampling.

RF_oversampling_human_proteomics_6M.R: Run random forests on human proteomics data of visit 6 months applying the default settings and using oversampling.

RF_oversampling_human_proteomics_12M.R: Run random forests on human proteomics data of visit 12 months applying the default settings and using oversampling.

RF_oversampling_immune_all.R: Run random forests on immune data (all visits) applying the default settings and using oversampling.

RF_oversampling_immune_0M.R: Run random forests on immune data of the baseline visit applying the default settings and using oversampling.

RF_oversampling_immune_6M.R: Run random forests on immune data of visit 6 months applying the default settings and using oversampling.

RF_oversampling_immune_12M.R: Run random forests on immune data of visit 12 months applying the default settings and using oversampling.

RF_oversampling_metabolomics_polarN_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits) applying the default settings and using oversampling.

RF_oversampling_metabolomics_polarN_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for the baseline visit applying the default settings and using oversampling.

RF_oversampling_metabolomics_polarN_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 6 months applying the default settings and using oversampling.

RF_oversampling_metabolomics_polarN_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 12 months applying the default settings and using oversampling.

RF_oversampling_metabolomics_polarP_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits) applying the default settings and using oversampling.

RF_oversampling_metabolomics_polarP_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from the baseline visit applying the default settings and using oversampling.

RF_oversampling_metabolomics_polarP_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 6 months applying the default settings and using oversampling.

RF_oversampling_metabolomics_polarP_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 12 months applying the default settings and using oversampling.

RF_oversampling_metabolomics_lipid_all.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids (all visits) applying the default settings and using oversampling.

RF_oversampling_metabolomics_lipid_0M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for the baseline visit applying the default settings and using oversampling.

RF_oversampling_metabolomics_lipid_6M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 6 months applying the default settings and using oversampling.

RF_oversampling_metabolomics_lipid_12M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 12 months applying the default settings and using oversampling.

RF_oversampling_combine_models_all_visits.R: Combine the predictions of the models using all visits, default settings and oversampling.

RF_oversampling_combine_models_per_visit.R: Combine the predictions of the models per visit, using default settings and oversampling.


***folder influence_smote***
  
RF_smote_clinical_all.R: Run random forests on clinical data (all visits) applying the default settings and using smote.

RF_smote_clinical_0M.R: Run random forests on clinical data from the baseline visit applying the default settings and using smote.

=> not enough samples to apply smote to models per visit, code generates warnings and an error.

RF_smote_16SrRNAseq_all.R: Run random forests on 16S rRNA gene sequencing data (all visits) applying the default settings and using smote.

RF_smote_microb_metaproteomics_all.R: Run random forests on microbial metaproteomics data (all visits) applying the default settings and using smote. 

RF_smote_human_proteomics_all.R: Run random forests on human proteomics data (all visits) applying the default settings and using smote.

RF_smote_immune_all.R: Run random forests on immune data (all visits) applying the default settings and using smote.

RF_smote_metabolomics_polarN_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits) applying the default settings and using smote.

RF_smote_metabolomics_polarP_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits) applying the default settings and using smote.

RF_smote_metabolomics_lipid_all.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids (all visits) applying the default settings and using smote.

RF_smote_combine_models_all_visits.R: Combine the predictions of the models using all visits, default settings and smote.


***folder filter_near_zero_variance***
  
RF_nzv_clinical_all.R: Run random forests on clinical data (all visits) applying the default settings and removing variables with near zero variance.

RF_nzv_clinical_0M.R: Run random forests on clinical data from the baseline visit applying the default settings and removing variables with near zero variance.

RF_nzv_clinical_6M.R: Run random forests on clinical data from visit 6 months applying the default settings and removing variables with near zero variance.

RF_nzv_clinical_12M.R: Run random forests on clinical data from visit 12 months applying the default settings and removing variables with near zero variance.

RF_nzv_16SrRNAseq_all.R: Run random forests on 16S rRNA gene sequencing data (all visits) applying the default settings and removing variables with near zero variance.

RF_nzv_16SrRNAseq_0M.R: Run random forests on 16S rRNA gene sequencing data from the baseline visit applying the default settings and removing variables with near zero variance.

RF_nzv_16SrRNAseq_6M.R: Run random forests on 16S rRNA gene sequencing data from the visit 6 months applying the default settings and removing variables with near zero variance.

RF_nzv_16SrRNAseq_12M.R: Run random forests on 16S rRNA gene sequencing data from the visit 12 months applying the default settings and removing variables with near zero variance.

RF_nzv_microb_metaproteomics_all.R: Run random forests on microbial metaproteomics data (all visits) applying the default settings and removing variables with near zero variance. 

RF_nzv_microb_metaproteomics_0M.R: Run random forests on microbial metaproteomics data of the baseline visit applying the default settings and removing variables with near zero variance.

RF_nzv_microb_metaproteomics_6M.R: Run random forests on microbial metaproteomics data of visit 6 months applying the default settings and removing variables with near zero variance.

RF_nzv_microb_metaproteomics_12M.R: Run random forests on microbial metaproteomics data of visit 12 months applying the default settings and removing variables with near zero variance.

RF_nzv_human_proteomics_all.R: Run random forests on human proteomics data (all visits) applying the default settings and removing variables with near zero variance.

RF_nzv_human_proteomics_0M.R: Run random forests on human proteomics data of the baseline visit applying the default settings and removing variables with near zero variance.

RF_nzv_human_proteomics_6M.R: Run random forests on human proteomics data of visit 6 months applying the default settings and removing variables with near zero variance.

RF_nzv_human_proteomics_12M.R: Run random forests on human proteomics data of visit 12 months applying the default settings and removing variables with near zero variance.

RF_nzv_immune_all.R: Run random forests on immune data (all visits) applying the default settings and removing variables with near zero variance.

RF_nzv_immune_0M.R: Run random forests on immune data of the baseline visit applying the default settings and removing variables with near zero variance.

RF_nzv_immune_6M.R: Run random forests on immune data of visit 6 months applying the default settings and removing variables with near zero variance.

RF_nzv_immune_12M.R: Run random forests on immune data of visit 12 months applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_polarN_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits) applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_polarN_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for the baseline visit applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_polarN_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 6 months applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_polarN_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 12 months applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_polarP_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits) applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_polarP_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from the baseline visit applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_polarP_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 6 months applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_polarP_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 12 months applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_lipid_all.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids (all visits) applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_lipid_0M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for the baseline visit applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_lipid_6M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 6 months applying the default settings and removing variables with near zero variance.

RF_nzv_metabolomics_lipid_12M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 12 months applying the default settings and removing variables with near zero variance.

RF_nzv_combine_models_all_visits.R: Combine the predictions of the models using all visits, default settings and removing variables with near zero variance.

RF_nzv_combine_models_per_visit.R: Combine the predictions of the models per visit, using default settings and removing variables with near zero variance.


***folder filter_highly_correlated_predictors***
  
RF_corr_clinical_all.R: Run random forests on clinical data (all visits) applying the default settings and removing highly correlated predictors

RF_corr_clinical_0M.R: Run random forests on clinical data from the baseline visit applying the default settings and removing highly correlated predictors

RF_corr_clinical_6M.R: Run random forests on clinical data from visit 6 months applying the default settings and removing highly correlated predictors

RF_corr_clinical_12M.R: Run random forests on clinical data from visit 12 months applying the default settings and removing highly correlated predictors

RF_corr_16SrRNAseq_all.R: Run random forests on 16S rRNA gene sequencing data (all visits) applying the default settings and removing highly correlated predictors

RF_corr_16SrRNAseq_0M.R: Run random forests on 16S rRNA gene sequencing data from the baseline visit applying the default settings and removing highly correlated predictors

RF_corr_16SrRNAseq_6M.R: Run random forests on 16S rRNA gene sequencing data from the visit 6 months applying the default settings and removing highly correlated predictors

RF_corr_16SrRNAseq_12M.R: Run random forests on 16S rRNA gene sequencing data from the visit 12 months applying the default settings and removing highly correlated predictors

RF_corr_microb_metaproteomics_all.R: Run random forests on microbial metaproteomics data (all visits) applying the default settings and removing highly correlated predictors 

RF_corr_microb_metaproteomics_0M.R: Run random forests on microbial metaproteomics data of the baseline visit applying the default settings and removing highly correlated predictors

RF_corr_microb_metaproteomics_6M.R: Run random forests on microbial metaproteomics data of visit 6 months applying the default settings and removing highly correlated predictors

RF_corr_microb_metaproteomics_12M.R: Run random forests on microbial metaproteomics data of visit 12 months applying the default settings and removing highly correlated predictors

RF_corr_human_proteomics_all.R: Run random forests on human proteomics data (all visits) applying the default settings and removing highly correlated predictors

RF_corr_human_proteomics_0M.R: Run random forests on human proteomics data of the baseline visit applying the default settings and removing highly correlated predictors

RF_corr_human_proteomics_6M.R: Run random forests on human proteomics data of visit 6 months applying the default settings and removing highly correlated predictors

RF_corr_human_proteomics_12M.R: Run random forests on human proteomics data of visit 12 months applying the default settings and removing highly correlated predictors

RF_corr_immune_all.R: Run random forests on immune data (all visits) applying the default settings and removing highly correlated predictors

RF_corr_immune_0M.R: Run random forests on immune data of the baseline visit applying the default settings and removing highly correlated predictors

RF_corr_immune_6M.R: Run random forests on immune data of visit 6 months applying the default settings and removing highly correlated predictors

RF_corr_immune_12M.R: Run random forests on immune data of visit 12 months applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_polarN_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits) applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_polarN_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for the baseline visit applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_polarN_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 6 months applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_polarN_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 12 months applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_polarP_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits) applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_polarP_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from the baseline visit applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_polarP_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 6 months applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_polarP_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 12 months applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_lipid_all.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids (all visits) applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_lipid_0M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for the baseline visit applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_lipid_6M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 6 months applying the default settings and removing highly correlated predictors

RF_corr_metabolomics_lipid_12M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 12 months applying the default settings and removing highly correlated predictors

RF_corr_combine_models_all_visits.R: Combine the predictions of the models using all visits, default settings and removing highly correlated predictors

RF_corr_combine_models_per_visit.R: Combine the predictions of the models per visit, using default settings and removing highly correlated predictors


***Folder determine optimal mtry***
Fit the value for mtry for a given value of ntree.
To find optimal ntree, change the value of ntree in scripts below and compare the performance for different ntree.

RF_mtry_clinical_all.R: Fit mtry on random forests on clinical data (all visits).

RF_mtry_clinical_0M.R: Fit mtry on random forests on clinical data for the baseline visit.  

RF_mtry_clinical_6M.R: Fit mtry on random forests on clinical data for visit 6 months.  

RF_mtry_clinical_12M.R: Fit mtry on random forests on clinical data for visit 12 months.  

RF_mtry_16SrRNAseq_all.R: Fit mtry on random forests on 16S rRNA gene sequencing data (all visits).

RF_mtry_16SrRNAseq_0M.R: Fit mtry on random forests on 16S rRNA gene sequencing data for the baseline visit. 

RF_mtry_16SrRNAseq_6M.R: Fit mtry on random forests on 16S rRNA gene sequencing data from visit 6 months.

RF_mtry_16SrRNAseq_12M.R: Fit mtry on random forests on 16S rRNA gene sequencing data from visit 12 months.

RF_mtry_microb_metaproteomics_all.R: Fit mtry on random forests on microbial metaproteomics data (all visits).

RF_mtry_microb_metaproteomics_0M.R: Fit mtry on random forests on microbial metaproteomics data of the baseline visit.

RF_mtry_microb_metaproteomics_6M.R: Fit mtry on random forests on microbial metaproteomics data of visit 6 months.

RF_mtry_microb_metaproteomics_12M.R: Fit mtry on random forests on microbial metaproteomics data of visit 12 months.

RF_mtry_human_proteomics_all.R: Fit mtry on random forests on human proteomics data (all visits). 

RF_mtry_human_proteomics_0M.R: Fit mtry on random forests on human proteomics data of the baseline visit.

RF_mtry_human_proteomics_6M.R: Fit mtry on random forests on human proteomics data of visit 6 months.

RF_mtry_human_proteomics_12M.R: Fit mtry on random forests on human proteomics data of visit 12 months.

RF_mtry_immune_all.R: Fit mtry to random forests on immune data (all visits). 

RF_mtry_immune_0M.R: Fit mtry on random forests on immune data of the baseline visit.  

RF_mtry_immune_6M.R: Fit mtry on random forests on immune data of visit 6 months.   

RF_mtry_immune_12M.R: Fit mtry on random forests on immune data of visit 12 months.  

RF_mtry_metabolomics_polarN_all.R: Fit mtry on random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits). 

RF_mtry_metabolomics_polarN_0M.R: Fit mtry on random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for the baseline visit. 

RF_mtry_metabolomics_polarN_6M.R: Fit mtry on random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 6 months.   

RF_mtry_metabolomics_polarN_12M.R: Fit mtry on random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 12 months.    

RF_mtry_metabolomics_polarP_all.R: Fit mtry on random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits). 

RF_mtry_metabolomics_polarP_0M.R: Fit mtry on random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from the baseline visit.

RF_mtry_metabolomics_polarP_6M.R: Fit mtry on random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 6 months.

RF_mtry_metabolomics_polarP_12M.R: Fit mtry on random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 12 months.

RF_mtry_metabolomics_lipid_all.R: Fit mtry to random forests on metabolomics data from the platform for bile acids and fatty acids (all visits). 

RF_mtry_metabolomics_lipid_0M.R: Fit mtry on random forests on metabolomics data from the platform for bile acids and fatty acids for the baseline visit.     

RF_mtry_metabolomics_lipid_6M.R: Fit mtry on random forests on metabolomics data from the platform for bile acids and fatty acids for visit 6 months. 

RF_mtry_metabolomics_lipid_12M.R: Fit mtry on random forests on metabolomics data from the platform for bile acids and fatty acids for visit 12 months. 


***Folder run_models_with_optimal_mtry_and_ntree***
RF_optimal_clinical_all.R: Run random forests on clinical data (all visits) applying the optimal settings.

RF_optimal_clinical_0M.R: Run random forests on clinical data from the baseline visit applying the optimal settings. 

RF_optimal_clinical_6M.R: Run random forests on clinical data from visit 6 months applying the optimal settings.

RF_optimal_clinical_12M.R: Run random forests on clinical data from visit 12 months applying the optimal settings.

RF_optimal_16SrRNAseq_all.R: Run random forests on 16S rRNA gene sequencing data (all visits) applying the optimal settings.

RF_optimal_16SrRNAseq_0M.R: Run random forests on 16S rRNA gene sequencing data from the baseline visit applying the optimal settings.

RF_optimal_16SrRNAseq_6M.R: Run random forests on 16S rRNA gene sequencing data from the visit 6 months applying the optimal settings. 

RF_optimal_16SrRNAseq_12M.R: Run random forests on 16S rRNA gene sequencing data from the visit 12 months applying the optimal settings. 

RF_optimal_microb_metaproteomics_all.R: Run random forests on microbial metaproteomics data (all visits) applying the optimal settings. 

RF_optimal_microb_metaproteomics_0M.R: Run random forests on microbial metaproteomics data of the baseline visit applying the optimal settings. 

RF_optimal_microb_metaproteomics_6M.R: Run random forests on microbial metaproteomics data of visit 6 months applying the optimal settings.

RF_optimal_microb_metaproteomics_12M.R: Run random forests on microbial metaproteomics data of visit 12 months applying the optimal settings.

RF_optimal_human_proteomics_all.R: Run random forests on human proteomics data (all visits) applying the optimal settings. 

RF_optimal_human_proteomics_0M.R: Run random forests on human proteomics data of the baseline visit applying the optimal settings.

RF_optimal_human_proteomics_6M.R: Run random forests on human proteomics data of visit 6 months applying the optimal settings.

RF_optimal_human_proteomics_12M.R: Run random forests on human proteomics data of visit 12 months applying the optimal settings.

RF_optimal_immune_all.R: Run random forests on immune data (all visits) applying the optimal settings.

RF_optimal_immune_0M.R: Run random forests on immune data of the baseline visit applying the optimal settings.

RF_optimal_immune_6M.R: Run random forests on immune data of visit 6 months applying the optimal settings.

RF_optimal_immune_12M.R: Run random forests on immune data of visit 6 months applying the optimal settings.

RF_optimal_metabolomics_polarN_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits) applying the optimal settings.

RF_optimal_metabolomics_polarN_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for the baseline visit applying the optimal settings.

RF_optimal_metabolomics_polarN_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 6 months applying the optimal settings.

RF_optimal_metabolomics_polarN_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 12 months applying the optimal settings.

RF_optimal_metabolomics_polarP_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits) applying the optimal settings.

RF_optimal_metabolomics_polarP_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from the baseline visit applying the optimal settings. 

RF_optimal_metabolomics_polarP_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 6 months applying the optimal settings.  

RF_optimal_metabolomics_polarP_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 12 months applying the optimal settings.  

RF_optimal_metabolomics_lipid_all.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids (all visits) applying the optimal settings. 

RF_optimal_metabolomics_lipid_0M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for the baseline visit applying the optimal settings. 

RF_optimal_metabolomics_lipid_6M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 6 months applying the optimal settings. 

RF_optimal_metabolomics_lipid_12M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 12 months applying the optimal settings. 

RF_optimal_combine_models_all_visits.R: Combine the predictions of the models using all visits and optimal settings.

RF_optimal_combine_models_per_visit.R: Combine the predictions of the models per visit and optimal settings.


***Folder optimize_decision_threshold***
RF_decision_thresh_clinical_all.R: Determine optimal decision threshold for random forests on clinical data (all visits). 

RF_decision_thresh_clinical_0M.R: Determine optimal decision threshold for random forests on clinical data for the baseline visit. 

RF_decision_thresh_clinical_6M.R: Determine optimal decision threshold for random forests on clinical data for visit 6 months. 

RF_decision_thresh_clinical_12M.R: Determine optimal decision threshold for random forests on clinical data for visit 12 months. 

RF_decision_thresh_16SrRNAseq_all.R: Determine optimal decision threshold for random forests on 16S rRNA gene sequencing data (all visits).

RF_decision_thresh_16SrRNAseq_0M.R: Determine optimal decision threshold for random forests on 16S rRNA gene sequencing data from the baseline visit. 

RF_decision_thresh_16SrRNAseq_6M.R: Determine optimal decision threshold for random forests on 16S rRNA gene sequencing data from visit 6 months.

RF_decision_thresh_16SrRNAseq_12M.R: Determine optimal decision threshold for random forests on 16S rRNA gene sequencing data from visit 12 months.

RF_decision_thresh_microb_metaproteomics_all.R: Determine optimal decision threshold for random forests on microbial metaproteomics data (all visits). 

RF_decision_thresh_microb_metaproteomics_0M.R: Determine optimal decision threshold for random forests on microbial metaproteomics data of the baseline visit.

RF_decision_thresh_microb_metaproteomics_6M.R: Determine optimal decision threshold for random forests on microbial metaproteomics data of visit 6 months.

RF_decision_thresh_microb_metaproteomics_12M.R: Determine optimal decision threshold for random forests on microbial metaproteomics data of visit 12 months.

RF_decision_thresh_human_proteomics_all.R: Determine optimal decision threshold for random forests on human proteomics data (all visits). 

RF_decision_thresh_human_proteomics_0M.R: Determine optimal decision threshold for random forests on human proteomics data of the baseline visit.

RF_decision_thresh_human_proteomics_6M.R: Determine optimal decision threshold for random forests on human proteomics data of visit 6 months. 

RF_decision_thresh_human_proteomics_12M.R: Determine optimal decision threshold for random forests on human proteomics data of visit 12 months. 

RF_decision_thresh_immune_all.R: Determine optimal decision threshold for random forests on immune data (all visits).  

RF_decision_thresh_immune_0M.R: Determine optimal decision threshold for random forests on immune data of the baseline visit.

RF_decision_thresh_immune_6M.R: Determine optimal decision threshold for random forests on immune data of visit 6 months. 

RF_decision_thresh_immune_12M.R: Determine optimal decision threshold for random forests on immune data of visit 12 months.

RF_decision_thresh_metabolomics_polarN_all.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits).

RF_decision_thresh_metabolomics_polarN_0M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for the baseline visit. 

RF_decision_thresh_metabolomics_polarN_6M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 6 months. 

RF_decision_thresh_metabolomics_polarN_12M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 12 months. 

RF_decision_thresh_metabolomics_polarP_all.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits).  

RF_decision_thresh_metabolomics_polarP_0M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from the baseline visit. 

RF_decision_thresh_metabolomics_polarP_6M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 6 months. 

RF_decision_thresh_metabolomics_polarP_12M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 12 months. 

RF_decision_thresh_metabolomics_lipid_all.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for bile acids and fatty acids (all visits).

RF_decision_thresh_metabolomics_lipid_0M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for bile acids and fatty acids for the baseline visit.   

RF_decision_thresh_metabolomics_lipid_6M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for bile acids and fatty acids for visit 6 months.   

RF_decision_thresh_metabolomics_lipid_12M.R: Determine optimal decision threshold for random forests on metabolomics data from the platform for bile acids and fatty acids for visit 12 months.   


***Folder run_with_optimal_decision_threshold***
RF_opt_thresh_clinical_all.R: Run random forests on clinical data (all visits) applying the optimal settings and decision threshold.

RF_opt_thresh_clinical_0M.R: Run random forests on clinical data from the baseline visit applying the optimal settings and decision threshold. 

RF_opt_thresh_clinical_6M.R: Run random forests on clinical data from visit 6 months applying the optimal settings and decision threshold.

RF_opt_thresh_clinical_12M.R: Run random forests on clinical data from visit 12 months applying the optimal settings and decision threshold.

RF_opt_thresh_16SrRNAseq_all.R: Run random forests on 16S rRNA gene sequencing data (all visits) applying the optimal settings and decision threshold.

RF_opt_thresh_16SrRNAseq_0M.R: Run random forests on 16S rRNA gene sequencing data from the baseline visit applying the optimal settings and decision threshold.

RF_opt_thresh_16SrRNAseq_6M.R: Run random forests on 16S rRNA gene sequencing data from the visit 6 months applying the optimal settings and decision threshold. 

RF_opt_thresh_16SrRNAseq_12M.R: Run random forests on 16S rRNA gene sequencing data from the visit 12 months applying the optimal settings and decision threshold. 

RF_opt_thresh_microb_metaproteomics_all.R: Run random forests on microbial metaproteomics data (all visits) applying the optimal settings and decision threshold.

RF_opt_thresh_microb_metaproteomics_0M.R: Run random forests on microbial metaproteomics data of the baseline visit applying the optimal settings and decision threshold.

RF_opt_thresh_microb_metaproteomics_6M.R: Run random forests on microbial metaproteomics data of visit 6 months applying the optimal settings and decision threshold.

RF_opt_thresh_microb_metaproteomics_12M.R: Run random forests on microbial metaproteomics data of visit 12 months applying the optimal settings and decision threshold.

RF_opt_thresh_human_proteomics_all.R: Run random forests on human proteomics data (all visits) applying the optimal settings and decision threshold.

RF_opt_thresh_human_proteomics_0M.R: Run random forests on human proteomics data of the baseline visit applying the optimal settings and decision threshold.

RF_opt_thresh_human_proteomics_6M.R: Run random forests on human proteomics data of visit 6 months applying the optimal settings and decision threshold. 

RF_opt_thresh_human_proteomics_12M.R: Run random forests on human proteomics data of visit 12 months applying the optimal settings and decision threshold. 

RF_opt_thresh_immune_all.R: Run random forests on immune data (all visits) applying the optimal settings and decision threshold. 

RF_opt_thresh_immune_0M.R: Run random forests on immune data of the baseline visit applying the optimal settings and decision threshold. 

RF_opt_thresh_immune_6M.R: Run random forests on immune data of visit 6 months applying the optimal settings and decision threshold.

RF_opt_thresh_immune_12M.R: Run random forests on immune data of visit 12 months applying the optimal settings and decision threshold.  

RF_opt_thresh_metabolomics_polarN_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode (all visits) applying the optimal settings and decision threshold.

RF_opt_thresh_metabolomics_polarN_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for the baseline visit applying the optimal settings and decision threshold. 

RF_opt_thresh_metabolomics_polarN_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 6 months applying the optimal settings and decision threshold.

RF_opt_thresh_metabolomics_polarN_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - negative mode for visit 12 months applying the optimal settings and decision threshold.

RF_opt_thresh_metabolomics_polarP_all.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode (all visits) applying the optimal settings and decision threshold.

RF_opt_thresh_metabolomics_polarP_0M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from the baseline visit applying the optimal settings and decision threshold. 

RF_opt_thresh_metabolomics_polarP_6M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 6 months applying the optimal settings and decision threshold.

RF_opt_thresh_metabolomics_polarP_12M.R: Run random forests on metabolomics data from the platform for polar and semi-polar compounds - positive mode from visit 12 months applying the optimal settings and decision threshold.

RF_opt_thresh_metabolomics_lipid_all.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids (all visits) applying the optimal settings and decision threshold.   

RF_opt_thresh_metabolomics_lipid_0M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for the baseline visit applying the optimal settings and decision threshold.

RF_opt_thresh_metabolomics_lipid_6M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 6 months applying the optimal settings and decision threshold.   

RF_opt_thresh_metabolomics_lipid_12M.R: Run random forests on metabolomics data from the platform for bile acids and fatty acids for visit 12 months applying the optimal settings and decision threshold.   

RF_opt_thresh_combine_models_all_visits.R: Combine the predictions of the models using all visits and optimal settings and decision threshold.

RF_opt_combine_models_per_visit.R: Combine the predictions of the models per visit and optimal settings and decision threshold.

Approach1_clin_16S.R: Approach 1 (see manuscript): Clinical data + 16S rRNA seq.

Approach1_clin_microb_metaprot.R: Approach 1 (see manuscript): Clinical data + metaproteomics microbial.

Approach1_clin_microb_metaprot_human_prot.R: Approach 1 (see manuscript): Clinical data + metaproteomics microbial + metaproteomics human

Approach1_clin_microb_metaprot_immune.R: Approach 1 (see manuscript): Clinical data + metaproteomics microbial + immune data

Approach1_clin_microb_metaprot_metab_polarN.R: Approach 1 (see manuscript): Clinical data + metaproteomics microbial + metabolomics polarN 

Approach1_clin_microb_metaprot_metab_polarN_metab_polarP.R: Approach 1 (see manuscript): Clinical data + metaproteomics microbial + metabolomics polarN + metabolomics  polarP 

Approach1_clin_microb_metaprot_metab_polarN_metab_polarP_metab_lipid.R: Approach 1 (see manuscript): Clinical data + metaproteomics microbial + metabolomics polarN + metabolomics  polarP + metabolomics lipid

Approach2_polarP12M_clin0M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 0M  

Approach2_polarP12M_clin6M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M 

Approach2_polarP12M_clin6M_clin12M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + clinical data 12M 

Approach2_polarP12M_clin6M_16S0M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M

Approach2_polarP12M_clin6M_16S0M_16S6M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + 16S rRNAseq 6M 

Approach2_polarP12M_clin6M_16S0M_16S12M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + 16S rRNAseq 12M 

Approach2_polarP12M_clin6M_16S0M_micprot0M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M 

Approach2_polarP12M_clin6M_16S0M_micprot0M_micprot6M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + metaproteomics microbial 6M

Approach2_polarP12M_clin6M_16S0M_micprot0M_micprot12M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + metaproteomics microbial 12M

Approach2_polarP12M_clin6M_16S0M_micprot0M_humprot0M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + proteomics human 0M.

Approach2_polarP12M_clin6M_16S0M_micprot0M_humprot6M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + proteomics human 6M.

Approach2_polarP12M_clin6M_16S0M_micprot0M_humprot12M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + proteomics human 12M.

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune0M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 0M

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_immune12M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + immune data 12M

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_polarN0M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics polarN 0M

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_polarN6M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics polarN 6M

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_polarN6M_polarN12M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics polarN 6M + metabolomics polarN 12M

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_polarN6M_polarP0M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics polarN 6M + metabolomics  polarP 0M

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_polarN6M_polarP6M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics polarN 6M + metabolomics  polarP 6M

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_polarN6M_polarP6M_lipid0M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics polarN 6M + metabolomics  polarP 6M + metabolomics lipid 0M  

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_polarN6M_polarP6M_lipid6M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics polarN 6M + metabolomics  polarP 6M + metabolomics lipid 6M 

Approach2_polarP12M_clin6M_16S0M_micprot0M_immune6M_polarN6M_polarP6M_lipid12M.R: Approach 2 (see manuscript): Metabolomics  polarP 12M + clinical data 6M + 16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics polarN 6M + metabolomics  polarP 6M + metabolomics lipid 12M   


***Folder variable_importance_Gini_index***
Calculation of the variable importance - Gini index for each view used in the best model.
varimp_gini_clin_6M.R: Calculates variable importance (Gini index) for random forests model fitted to the clinical data at 6M. 

varimp_gini_16SrRNAseq_0M.R: Calculates variable importance (Gini index) for random forests model fitted to the 16S rRNA gene sequencing data at baseline. 

varimp_gini_microb_metaproteomics_0M.R: Calculates variable importance (Gini index) for random forests model fitted to the microbial metaproteomics data at baseline. 

varimp_gini_immune_6M.R: Calculates variable importance (Gini index) for random forests model fitted to the immune data at 6M.

varimp_gini_metabolomics_polarN_6M.R: Calculates variable importance (Gini index) for random forests model fitted to the metabolomics data - platform for polar and semi-polar compounds - negative mode at 6M. 

varimp_gini_metabolomics_polarN_6M.R: Calculates variable importance (Gini index) for random forests model fitted to the metabolomics data - platform for polar and semi-polar compounds - positive mode at 6M.  

varimp_gini_metabolomics_polarN_12M.R: Calculates variable importance (Gini index) for random forests model fitted to the metabolomics data - platform for polar and semi-polar compounds - positive mode at 12M.  


***Folder permutation_based_variable_importance***
Calculation of the permutation-based variable importance for each view used in the best model.
perm_based_varimp_clin_6M.R: Calculates permutation-based variable importance for random forests model fitted to the clinical data at 6M.

perm_based_varimp_16SrRNAseq_0M.R: Calculates permutation-based variable importance for random forests model fitted to the 16S rRNA gene sequencing data at baseline.

perm_based_varimp_microb_metaproteomics_0M.R: Calculates permutation-based variable importance for random forests model fitted to the microbial metaproteomics data at baseline. 

perm_based_varimp_immune_6M.R: Calculates permutation-based variable importance for random forests model fitted to the immune data at 6M.

perm_based_varimp_metabolomics_polarN_6M.R: Calculates permutation-based variable importance for random forests model fitted to the metabolomics data - platform for polar and semi-polar compounds - negative mode at 6M.

perm_based_varimp_metabolomics_polarN_6M.R: Calculates permutation-based variable importance for random forests model fitted to the metabolomics data - platform for polar and semi-polar compounds - positive mode at 6M.

perm_based_varimp_metabolomics_polarN_12M.R: Calculates permutation-based variable importance for random forests model fitted to the metabolomics data - platform for polar and semi-polar compounds - positive mode at 12M.






 