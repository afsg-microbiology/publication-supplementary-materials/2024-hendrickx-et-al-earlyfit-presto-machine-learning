################################################################################
#Approach 2 (see manuscript): Metabolomics polarP 12M + clinical data 6M +     #
#16S rRNAseq 0M + metaproteomics microbial 0M  + immune data 6M + metabolomics #
#polarN 0M                                                                     #
################################################################################
#Author: Diana Hendrickx#
#########################
#input
#results_RF_optthresh_0M_train_ntree_opt.xlsx for each view: Excel output from 
#running the R scripts RF_opt_thresh_[view]_0M.R
#RF_optthresh_0M_ntree_opt.RData for each view: workspace from running the R 
#scripts RF_opt_thresh_[view]_0M.R
#results_RF_optthresh_6M_train_ntree_opt.xlsx for each view: Excel output from 
#running the R scripts RF_opt_thresh_[view]_6M.R
#RF_optthresh_6M_ntree_opt.RData for each view: workspace from running the R 
#scripts RF_opt_thresh_[view]_6M.R
#results_RF_optthresh_12M_train_ntree_opt.xlsx for each view: Excel output from 
#running the R scripts RF_opt_thresh_[view]_12M.R
#RF_optthresh_12M_ntree_opt.RData for each view: workspace from running the R 
#scripts RF_opt_thresh_[view]_12M.R

setwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")
rm(list = ls())

# Load R packages.
library(readxl)
library(precrec)
library(openxlsx)
library(dplyr)

# Calculate weight to be used for each model.
# The weight is based on the mean AUROC for the training sets.
# First load the results for each -omics and each visit.
results_clinical_6M <- read_excel("[workdirectory_clinical_data]/results_RF_optthresh_6M_train_ntree_opt.xlsx")

results_RF_16S_0M <- read_excel("[workdirectory_16SrRNAseq]/results_RF_optthresh_0M_train_ntree_opt.xlsx")

results_RF_microbial_proteins_0M <- read_excel("[workdirectory_metaproteomics]/results_RF_optthresh_microbial_0M_train_ntree_opt.xlsx")

results_RF_immunomics_6M <- read_excel("[workdirectory_immune_data]/results_RF_optthresh_6M_train_ntree_opt.xlsx")

results_RF_metabolomics_AGN_0M <- read_excel("[workdirectory_metabolomics]/results_RF_optthresh_AGN_0M_train_ntree_opt.xlsx")

results_RF_metabolomics_AGP_12M <- read_excel("[workdirectory_metabolomics]/results_RF_optthresh_AGP_12M_train_ntree_opt.xlsx")

#extract aucs
aucs<-c(results_clinical_6M$mean[1],
        results_RF_16S_0M$mean[1],
        results_RF_microbial_proteins_0M$mean[1],
        results_RF_immunomics_6M$mean[1],
        results_RF_metabolomics_AGN_0M$mean[1],
        results_RF_metabolomics_AGP_12M$mean[1])
#calculate weights
weights<-aucs/sum(aucs) #in this way the sum of the weights is 1

#load lists of subjects for test sets
subj_test1 <- read.table("subj_test1.txt", quote="\"", comment.char="")
subj_test2 <- read.table("subj_test2.txt", quote="\"", comment.char="")
subj_test3 <- read.table("subj_test3.txt", quote="\"", comment.char="")
subj_test4 <- read.table("subj_test4.txt", quote="\"", comment.char="")
subj_test5 <- read.table("subj_test5.txt", quote="\"", comment.char="")

#calculate performance of combined model on test sets
#calculate predicted probabilities for clinical data
load("[workdirectory_clinical_data]/RF_optthresh_6M_ntree_opt.RData")
df <- c()
for (i in 1:5){
  set.seed(123)
  df[[i]]<-  data.frame(obs = test_sets[[i]]$tolerance_12m)
  df[[i]]$prediction  <- (predict(RF_model[[i]], test_sets[[i]], 
                                  type = "prob"))
}
df_clinical_6M<-df

ind1<-which(subj_test1$V1 %in% c("[****]","[*****]","[******]"))
ind2<-which(subj_test2$V1 %in% c("[****]","[*****]","[******]"))
ind3<-which(subj_test3$V1 %in% c("[****]","[*****]","[******]"))
ind4<-which(subj_test4$V1 %in% c("[****]","[*****]","[******]"))
ind5<-which(subj_test5$V1 %in% c("[****]","[*****]","[******]"))

if (length(ind1)>=1){
  df_clinical_6M[[1]]<-df_clinical_6M[[1]][-ind1,]
}
if (length(ind2)>=1){
  df_clinical_6M[[2]]<-df_clinical_6M[[2]][-ind2,]
}
if (length(ind3)>=1){
  df_clinical_6M[[3]]<-df_clinical_6M[[3]][-ind3,]
}
if (length(ind4)>=1){
  df_clinical_6M[[4]]<-df_clinical_6M[[4]][-ind4,]
}
if (length(ind5)>=1){
  df_clinical_6M[[5]]<-df_clinical_6M[[5]][-ind5,]
}

#calculate predicted probabilities for 16S rRNAseq
load("[workdirectory_16SrRNAseq]/RF_optthresh_0M_ntree_opt.RData")
df <- c()
for (i in 1:5){
  set.seed(123)
  df[[i]]<-  data.frame(obs = test_sets[[i]]$tolerance_12m)
  df[[i]]$prediction  <- (predict(RF_model[[i]], test_sets[[i]], 
                                  type = "prob"))
}
df_16S_rRNAseq_0M<-df

if (length(ind1)>=1){
  df_16S_rRNAseq_0M[[1]]<-df_16S_rRNAseq_0M[[1]][-ind1,]
}
if (length(ind2)>=1){
  df_16S_rRNAseq_0M[[2]]<-df_16S_rRNAseq_0M[[2]][-ind2,]
}
if (length(ind3)>=1){
  df_16S_rRNAseq_0M[[3]]<-df_16S_rRNAseq_0M[[3]][-ind3,]
}
if (length(ind4)>=1){
  df_16S_rRNAseq_0M[[4]]<-df_16S_rRNAseq_0M[[4]][-ind4,]
}
if (length(ind5)>=1){
  df_16S_rRNAseq_0M[[5]]<-df_16S_rRNAseq_0M[[5]][-ind5,]
}

#calculate predicted probabilities for metaproteomics - microbial proteins
load("[workdirectory_metaproteomics]/RF_optthresh_microbial_0M_ntree_opt.RData")
df <- c()
for (i in 1:5){
  set.seed(123)
  df[[i]]<-  data.frame(obs = test_sets[[i]]$tolerance_12m)
  df[[i]]$prediction  <- (predict(RF_model[[i]], test_sets[[i]], 
                                  type = "prob"))
}
df_metaproteomics_microbial_0M<-df

if (length(ind1)>=1){
  df_metaproteomics_microbial_0M[[1]]<-df_metaproteomics_microbial_0M[[1]][-ind1,]
}
if (length(ind2)>=1){
  df_metaproteomics_microbial_0M[[2]]<-df_metaproteomics_microbial_0M[[2]][-ind2,]
}
if (length(ind3)>=1){
  df_metaproteomics_microbial_0M[[3]]<-df_metaproteomics_microbial_0M[[3]][-ind3,]
}
if (length(ind4)>=1){
  df_metaproteomics_microbial_0M[[4]]<-df_metaproteomics_microbial_0M[[4]][-ind4,]
}
if (length(ind5)>=1){
  df_metaproteomics_microbial_0M[[5]]<-df_metaproteomics_microbial_0M[[5]][-ind5,]
}

#calculate predicted probabilities for immunomics
load("[workdirectory_immune_data]/RF_optthresh_6M_ntree_opt.RData")
df <- c()
for (i in 1:5){
  set.seed(123)
  df[[i]]<-  data.frame(obs = test_sets[[i]]$allergy_status)
  df[[i]]$prediction  <- (predict(RF_model[[i]], test_sets[[i]], 
                                  type = "prob"))
}
df_immunomics_6M<-df

if (length(ind1)>=1){
  df_immunomics_6M[[1]]<-df_immunomics_6M[[1]][-ind1,]
}
if (length(ind2)>=1){
  df_immunomics_6M[[2]]<-df_immunomics_6M[[2]][-ind2,]
}
if (length(ind3)>=1){
  df_immunomics_6M[[3]]<-df_immunomics_6M[[3]][-ind3,]
}
if (length(ind4)>=1){
  df_immunomics_6M[[4]]<-df_immunomics_6M[[4]][-ind4,]
}
if (length(ind5)>=1){
  df_immunomics_6M[[5]]<-df_immunomics_6M[[5]][-ind5,]
}

#calculate predicted probabilities for metabolomics - AGN
load("[workdirectory_metabolomics]/RF_optthresh_AGN_0M_ntree_opt.RData")
df <- c()
for (i in 1:5){
  set.seed(123)
  df[[i]]<-  data.frame(obs = test_sets[[i]]$aval.CMA.12m)
  df[[i]]$prediction  <- (predict(RF_model[[i]], test_sets[[i]], 
                                  type = "prob"))
}
df_metabolomics_AGN_0M<-df

if (length(ind1)>=1){
  df_metabolomics_AGN_0M[[1]]<-df_metabolomics_AGN_0M[[1]][-ind1,]
}
if (length(ind2)>=1){
  df_metabolomics_AGN_0M[[2]]<-df_metabolomics_AGN_0M[[2]][-ind2,]
}
if (length(ind3)>=1){
  df_metabolomics_AGN_0M[[3]]<-df_metabolomics_AGN_0M[[3]][-ind3,]
}
if (length(ind4)>=1){
  df_metabolomics_AGN_0M[[4]]<-df_metabolomics_AGN_0M[[4]][-ind4,]
}
if (length(ind5)>=1){
  df_metabolomics_AGN_0M[[5]]<-df_metabolomics_AGN_0M[[5]][-ind5,]
}

#calculate predicted probabilities for metabolomics - AGP
load("[workdirectory_metabolomics]/RF_optthresh_AGP_12M_ntree_opt.RData")
df <- c()
for (i in 1:5){
  set.seed(123)
  df[[i]]<-  data.frame(obs = test_sets[[i]]$aval.CMA.12m)
  df[[i]]$prediction  <- (predict(RF_model[[i]], test_sets[[i]], 
                                  type = "prob"))
}
df_metabolomics_AGP_12M<-df

if (length(ind1)>=1){
  df_metabolomics_AGP_12M[[1]]<-df_metabolomics_AGP_12M[[1]][-ind1,]
}
if (length(ind2)>=1){
  df_metabolomics_AGP_12M[[2]]<-df_metabolomics_AGP_12M[[2]][-ind2,]
}
if (length(ind3)>=1){
  df_metabolomics_AGP_12M[[3]]<-df_metabolomics_AGP_12M[[3]][-ind3,]
}
if (length(ind4)>=1){
  df_metabolomics_AGP_12M[[4]]<-df_metabolomics_AGP_12M[[4]][-ind4,]
}
if (length(ind5)>=1){
  df_metabolomics_AGP_12M[[5]]<-df_metabolomics_AGP_12M[[5]][-ind5,]
}

#combine predicted probabilities
df_test<-c()
for (i in 1:5){
  set.seed(123)
  df_test[[i]]<- data.frame(obs = df_clinical_6M[[i]]$obs,
                            No = df_clinical_6M[[i]]$prediction$No*weights[1] +
                              df_16S_rRNAseq_0M[[i]]$prediction$No*weights[2] +
                              df_metaproteomics_microbial_0M[[i]]$prediction$No*weights[3] +
                              df_immunomics_6M[[i]]$prediction$allergic*weights[4] +
                              df_metabolomics_AGN_0M[[i]]$prediction$NotTolerant*weights[5] +
                              df_metabolomics_AGP_12M[[i]]$prediction$NotTolerant*weights[6],
                            Yes = df_clinical_6M[[i]]$prediction$Yes*weights[1] +
                              df_16S_rRNAseq_0M[[i]]$prediction$Yes*weights[2] +
                              df_metaproteomics_microbial_0M[[i]]$prediction$Yes*weights[3] +
                              df_immunomics_6M[[i]]$prediction$tolerant*weights[4] +
                              df_metabolomics_AGN_0M[[i]]$prediction$Tolerant*weights[5] +
                              df_metabolomics_AGP_12M[[i]]$prediction$Tolerant*weights[6])
}

results_combine_models_test<-data.frame(statistic=c("auc","sensitivity","specificity"))

results_test<-c()
for (i in 1:5){
  precrec_test <- evalmod(scores = df_test[[i]][,2:3],
                          labels = df_test[[i]]$obs,
                          posclass = "No")
  auc<-auc(precrec_test)$aucs[1]
  pred<-vector(mode="character",length=length(df_test[[i]]$No))
  pred[which(df_test[[i]]$No >= 0.40)]<-"No"
  pred[which(df_test[[i]]$No < 0.40)]<-"Yes"
  TP<-length(which(df_test[[i]]$obs=="No" & pred=="No"))
  FP<-length(which(df_test[[i]]$obs=="Yes" & pred=="No"))
  TN<-length(which(df_test[[i]]$obs=="Yes" & pred=="Yes"))
  FN<-length(which(df_test[[i]]$obs=="No" & pred=="Yes"))
  sensitivity<-TP/(TP+FN)
  specificity<-TN/(TN+FP)
  results_test[[i]]<-c(auc,sensitivity,specificity)
}

results_combine_models_test$set_1<-results_test[[1]]
results_combine_models_test$set_2<-results_test[[2]]
results_combine_models_test$set_3<-results_test[[3]]
results_combine_models_test$set_4<-results_test[[4]]
results_combine_models_test$set_5<-results_test[[5]]
results_combine_models_test$mean<-rowMeans(results_combine_models_test[,2:6])
results_combine_models_test$sd<-apply(results_combine_models_test[,2:6],1,sd)

write.xlsx(results_combine_models_test,"results_combine_models_per_visit_test_AGP_12M_clin_6M_16S_0M_metaprot_microb_0M_immun_6M_AGN_0M_ntree_opt.xlsx")

#save work space
save.image("combine_models_per_visit_AGP_12M_clin_6M_16S_0M_metaprot_microb_0M_immun_6M_AGN_0M_ntree_opt.RData")

#Get session info.
sessionInfo()