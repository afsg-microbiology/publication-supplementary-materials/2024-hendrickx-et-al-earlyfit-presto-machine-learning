################################################################################
#Extra pre-processing steps to put clinical data in the right format for ML.   #
################################################################################
#Author: Diana Hendrickx#
#########################
#input:                                                                        
#workspace_clindat.RData: an R workspace that includes the clinical data       
#collected at baseline (clindat_0M), at 6 months (clindat_6M), at 12 months    
#(clindat_12M), at all visits (clindat_all).                                   
#metadata.csv: csv file with columns sampleID,subjectnr,visit,treatment        
#Samples_Earlyfit.xlsx: Excel file with columns Sample ID, subjectnr, VisitN,  
#subj_visit, treatment, tolerant to CM at 12M.                                 

#set work directory
setwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")

#load R packages
library(readr)
library(readxl)
library(limma)
library(openxlsx)

#Select clinical data from previous R workspace.
load("workspace_clindat.RData")
rm(list=setdiff(ls(), c("clindat_0M", "clindat_6M", "clindat_12M", "clindat_all")))

#load additional metadata file (metadata.csv), order by sample ID
meta_data <- read_csv("metadata.csv", show_col_types = FALSE)
meta_data<-meta_data[order(meta_data$sampleID),]

#Remove the subject for which the allergy status at 12 months is unknown. This is subject [***].
ind_remove<-which(meta_data$subjectnr=="[***]")
meta_data<-meta_data[-ind_remove,]

#Add column with information on allergy status at 12 months
Samples_Earlyfit <- read_excel("Samples_Earlyfit.xlsx")
ind_remove<-which(Samples_Earlyfit$subjectnr=="[***]") #remove [***]
Samples_Earlyfit<-Samples_Earlyfit[-ind_remove,]
Samples_Earlyfit<-Samples_Earlyfit[order(Samples_Earlyfit$`Sample ID`),] #order by sampleID
DAS_nr<-paste0("DAS",strsplit2(as.character(meta_data$sampleID),split="DAS")[,2])
check<-which(Samples_Earlyfit$`Sample ID`!=DAS_nr) #check if the order is the same
print(check) #has to be integer(0)
meta_data<-data.frame(meta_data,tolerance_12m=Samples_Earlyfit$`tolerant to CM at 12M`)

#Make a dataframe with the metadata for all samples
ind_sample<-vector(mode="integer",length=117)
for (i in 1:117){
  ind_sample[i]<-which(rownames(clindat_all)==Samples_Earlyfit$subj_visit[i])
} #Put clindat_all in same order as Samples_Earlyfit
meta_data_all<-data.frame(sampleID=as.factor(meta_data$sampleID),
                          subjectnr=as.factor(meta_data$subjectnr),
                          visit=as.factor(meta_data$visit),
                          tolerance_12m=as.factor(meta_data$tolerance_12m),
                          clindat_all[ind_sample,])

#Make a dataframe with the metadata for the samples of visit 0M
meta_0M<-meta_data[which(meta_data$visit=="baseline"),]
Samples_Earlyfit_0M<-Samples_Earlyfit[which(Samples_Earlyfit$VisitN=="baseline"),]
ind_sample_0M<-vector(mode="integer",length=39)
for (i in 1:39){
  ind_sample_0M[i]<-which(rownames(clindat_0M)==Samples_Earlyfit_0M$subjectnr[i])
} #Put clindat_0M in same order as Samples_Earlyfit_0M
meta_data_0M<-data.frame(sampleID=as.factor(meta_0M$sampleID),
                         subjectnr=as.factor(meta_0M$subjectnr),
                         visit=as.factor(meta_0M$visit),
                         tolerance_12m=as.factor(meta_0M$tolerance_12m),
                         clindat_0M[ind_sample_0M,])

#Make a dataframe with the metadata for the samples of visit 6M
meta_6M<-meta_data[which(meta_data$visit=="T06M"),]
Samples_Earlyfit_6M<-Samples_Earlyfit[which(Samples_Earlyfit$VisitN=="T06M"),]
ind_sample_6M<-vector(mode="integer",length=39)
for (i in 1:39){
  ind_sample_6M[i]<-which(rownames(clindat_6M)==Samples_Earlyfit_6M$subjectnr[i])
} #Put clindat_6M in same order as Samples_Earlyfit_6M
meta_data_6M<-data.frame(sampleID=as.factor(meta_6M$sampleID),
                         subjectnr=as.factor(meta_6M$subjectnr),
                         visit=as.factor(meta_6M$visit),
                         tolerance_12m=as.factor(meta_6M$tolerance_12m),
                         clindat_6M[ind_sample_6M,])

#Make a dataframe with the metadata for the samples of visit 6M
meta_12M<-meta_data[which(meta_data$visit=="T12M"),]
Samples_Earlyfit_12M<-Samples_Earlyfit[which(Samples_Earlyfit$VisitN=="T12M"),]
ind_sample_12M<-vector(mode="integer",length=39)
for (i in 1:39){
  ind_sample_12M[i]<-which(rownames(clindat_12M)==Samples_Earlyfit_12M$subjectnr[i])
} #Put clindat_12M in same order as Samples_Earlyfit_12M
meta_data_12M<-data.frame(sampleID=as.factor(meta_12M$sampleID),
                          subjectnr=as.factor(meta_12M$subjectnr),
                          visit=as.factor(meta_12M$visit),
                          tolerance_12m=as.factor(meta_12M$tolerance_12m),
                          clindat_12M[ind_sample_12M,])


#write tables to Excel files
write.xlsx(meta_data_all,"meta_data_all.xlsx")
write.xlsx(meta_data_0M,"meta_data_0M.xlsx")
write.xlsx(meta_data_6M,"meta_data_6M.xlsx")
write.xlsx(meta_data_12M,"meta_data_12M.xlsx")

#save work space
save.image("preprocessing_for_ML.RData")

#Print session info
sessionInfo()







