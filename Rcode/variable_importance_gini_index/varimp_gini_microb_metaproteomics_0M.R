################################################################################
#Calculates variable importance (Gini index) for random forests model fitted to#
#the microbial metaproteomics data at baseline.                                #
################################################################################
#Author: Diana Hendrickx#
#########################
#input
#RF_optthresh_microbial_0M_ntree_opt.RData: workspace from running the script
#RF_opt_thresh_microb_metaproteomics_0M.R

setwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")
rm(list = ls())

library(caret)
library(openxlsx)

load("RF_optthresh_microbial_0M_ntree_opt.RData")

var_imp<-list()

for (i in 1:5){
  var_imp[[i]]<-varImp(RF_model[[i]],scale=FALSE)
}

var_imp_table<-data.frame(model1=var_imp[[1]]$importance$Overall,
                          model2=var_imp[[2]]$importance$Overall,
                          model3=var_imp[[3]]$importance$Overall,
                          model4=var_imp[[4]]$importance$Overall,
                          model5=var_imp[[5]]$importance$Overall)
var_imp_table<-data.frame(var_imp_table,
                          mean=rowMeans(var_imp_table),
                          sd=apply(var_imp_table,1,sd))
rownames(var_imp_table)<-rownames(var_imp[[1]]$importance)

#order by mean in decreasing order
sorted_var_imp_table<-var_imp_table[order(var_imp_table$mean,
                                          decreasing = TRUE),]

write.xlsx(sorted_var_imp_table,"sorted_var_imp_table_microbial_0M_opt.xlsx",rowNames=TRUE)

save.image("workspace_varimp_microbial_0M_opt.RData")

sessionInfo()